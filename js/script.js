"use strict";
/*
1. Змінну можна оголосити операторами let, const (preferable), var (deprecated).
2. Метод prompt() призначений для виведення діалогового вікна з повідомленням, текстовим полем для введення даних і кнопками 'Ok' та 'Cancel'.
Метод confirm() застосовується для виведення модального діалогового вікна з повідомленням та кнопками 'Ok' та 'Cancel' без текстового поля. Повертає результат true – якщо користувач натиснув на кнопку 'Ok'; false – в інших випадках.
3. JavaScript – це мова зі слабкою типізацією. Неявне перетворення типів - це автоматичний процес перетворення значень з одного типу до іншого (наприклад, рядка до числа або числа до рядка). Наприклад, console.log(18 / "3"), тут рядок "3" перетворюється на номер 3.
*/
let name = "Volodymyr";
const admin = name;
console.log(admin);
const days = 9;
const seconds = days * 24 * 60 * 60;
console.log(seconds);
let userEntry;
userEntry = prompt("Please enter something and press 'Ok':", "");
console.log(userEntry);
